public class CharacterArray
{
	public static void main(String[] args) 
	{		
		StringBuilder input = new StringBuilder("NVCC Manassas Campus 6901 Sudley Road Manassas VA 20109");
		String originalInput = input.toString();
		for (int i = 0; i < input.length(); i++) 
		{
			char index = input.charAt(i);
			if(Character.isDigit(input.charAt(i))) 
			{
				System.out.println(input.charAt(i)+" is a digit");
				input.replace(i, i+1,"*");
			}
			else if (Character.isLowerCase(input.charAt(i)))
			{
				System.out.println(input.charAt(i)+" is a lowercase character");
				input.setCharAt(i,Character.toUpperCase(index));
			}
			else if (Character.isUpperCase(input.charAt(i)))
			{
				System.out.println(input.charAt(i)+" is a UPPERCASE character");
				input.setCharAt(i,Character.toLowerCase(index));
			}
		}
		System.out.println("The original input is: "+originalInput);
		System.out.println("The new input is: "+input);		
	}
}